﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ASE_Hussain
{
    class Circle : Shape
    {
        /// <summary>
        /// Circle class extends the Shape interface property.
        /// summary of Circle Class
        /// <param name="inputStoring">Stores the input from the textbox of a Circle class</param>
        /// <param name="g">Graphics of the Circle class</param>
        /// <param name="x">First parameter of the Circle class method</param>
        /// <param name="y">Second parameter of the Circle class method</param>
        ///  <param name="colour">It makes user to choose colour</param>
        ///  

        Color pencolor = CommandParser.colour;
        private Color color;
        private (int, int) point;
        private float penWidth;
        private bool fillState;
        private int radius;
        private int x;
        private int y;

        public Circle()
        {
        }

        public Circle(Color color, (int, int) point, float penWidth, bool fillState, int radius)
        {
            this.color = color;
            this.point = point;
            this.penWidth = penWidth;
            this.fillState = fillState;
            this.radius = radius;
        }

        public Circle(Color color, int x, int y, float penWidth, bool fillState, int radius)
        {
            this.color = color;
            this.x = x;
            this.y = y;
            this.penWidth = penWidth;
            this.fillState = fillState;
            this.radius = radius;
        }

        public void drawShape(string[] inputStoring, Graphics g, int x, int y, Color colour)
        {
            if (CommandParser.isFillOn == true)
            {

                if (CommandParser.isPen == true)
                {
                    SolidBrush sb = new SolidBrush(pencolor);
                    int c = Convert.ToInt32(inputStoring[1]);
                    int d = Convert.ToInt32(inputStoring[1]);
                    Pen p = new Pen(pencolor, 2);
                    g.FillEllipse(sb, x, y, c, d);
                }
                else
                {
                    SolidBrush ab = new SolidBrush(Color.Black);
                    int c = Convert.ToInt32(inputStoring[1]);
                    int d = Convert.ToInt32(inputStoring[1]);
                    Pen p = new Pen(pencolor, 2);
                    g.FillEllipse(ab, x, y, c, d);
                }


            }
            else
            {
                if (CommandParser.isPen == true)
                {
                    int c = Convert.ToInt32(inputStoring[1]);
                    int d = Convert.ToInt32(inputStoring[1]);
                    Pen p = new Pen(pencolor, 2);
                    g.DrawEllipse(p, x, y, c, d);
                }
                else
                {
                    int c = Convert.ToInt32(inputStoring[1]);
                    int d = Convert.ToInt32(inputStoring[1]);
                    Pen p = new Pen(Color.Black, 2);
                    g.DrawEllipse(p, x, y, c, d);
                }
            }
        }

        public void flashRunner()
        {
            throw new NotImplementedException();
        }

        public void Paint(Graphics graphics)
        {
            throw new NotImplementedException();
        }

        internal void setFlash(Color secondColor)
        {
            throw new NotImplementedException();
        }
    }
}
