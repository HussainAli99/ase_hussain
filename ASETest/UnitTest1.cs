﻿using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Vml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace ASETest
{
    [TestClass]
    public class UnitTest1
    {
      
        // Triangle Test To see if shape drawn correctly
        [TestMethod]
        public void TriangleTest1()
        {
            DebugDrawingClass class1 = new DebugDrawingClass(new PictureBox());
            DebugDrawingClass class2 = new DebugDrawingClass(new PictureBox());
            CommandParser parser = new CommandParser(class1);

            parser.executeLine("Triangle 100,100 100,100,100,100");
            class2.drawTriangle((100, 100), (100, 100), (100, 100));

            CompareListOfShapes(class1.GetShapes(), class2.GetShapes(), 1);
        }

        private void CompareListOfShapes(List<DocumentFormat.OpenXml.Drawing.Shape> shapes1, List<DocumentFormat.OpenXml.Drawing.Shape> shapes2, int v)
        {
            throw new NotImplementedException();
        }


        //Rectangle Test to see if shape drawn correctly.
        [TestMethod]
        public void RectangleTest1()
        {
            DebugDrawingClass class1 = new DebugDrawingClass(new PictureBox());
            DebugDrawingClass class2 = new DebugDrawingClass(new PictureBox());
            CommandParser parser = new CommandParser(class1);

            string script = "position pen 50,50\n"
                + "rectangle 50 50\n"
                + "fill on\n"
                + "pen blue\n"
                + "rectangle 200,200 100 50";
            parser.executeScript(script);

            class2.setPosition(50, 50);
            class2.drawRectangle(25, 25);
            class2.setFillState(true);
            class2.setPenColour((0, 0, 255, 255));
            class2.drawRectangle(100, 100, 50, 25);

            CompareListOfShapes(class1.GetShapes(), class2.GetShapes(), 2);
        }
    }

    internal class PictureBox : Forms.PictureBox
    {
    }

    internal class CommandParser
    {
        private DebugDrawingClass class1;

        public CommandParser(DebugDrawingClass class1)
        {
            this.class1 = class1;
        }

        internal void executeLine(string v)
        {
            throw new NotImplementedException();
        }

        internal void executeScript(string script)
        {
            throw new NotImplementedException();
        }
    }

    internal class Panel
    {
        public Panel()
        {
        }
    }
}
