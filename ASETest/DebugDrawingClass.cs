﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using DocumentFormat.OpenXml.Drawing;
using System.Windows;
using static System.Windows.Forms;

namespace ASETest
{
    class DebugDrawingClass : DrawingClass
    {
        private List<Shape> shapes;
        private Panel panel;

        public PictureBox PictureBox { get; }

        public DebugDrawingClass(System.Windows.Forms.PictureBox pb) : base(pb)
        { }

        public DebugDrawingClass(PictureBox PictureBox)
        {
            this.PictureBox = PictureBox;
        }

        public List<Shape> GetShapes()
        {
            return shapes;
        }

        internal void drawTriangle((int, int) p1, (int, int) p2, (int, int) p3)
        {
            throw new NotImplementedException();
        }

        internal void setPosition(int v1, int v2)
        {
            throw new NotImplementedException();
        }

        internal void drawRectangle(int v1, int v2, int v, int v3)
        {
            throw new NotImplementedException();
        }

        internal void setFillState(bool v)
        {
            throw new NotImplementedException();
        }

        internal void setPenColour((int, int, int, int) p)
        {
            throw new NotImplementedException();
        }

        internal void drawRectangle(int v1, int v2)
        {
            throw new NotImplementedException();
        }
    }

    internal class DrawingClass
    {
        private Forms.Panel pl;
        private Forms.PictureBox pb;

        public DrawingClass(Forms.Panel pl)
        {
            this.pl = pl;
        }

        public DrawingClass(Forms.PictureBox pb)
        {
            this.pb = pb;
        }
    }

}
